# Playground for Kubernetes interactions

## Prepare the Docker Image

Following the commands necessary to build and run an image.
```
First: cd in the the project folder
 
// Build the image
docker build . -t minikube-example:latest

// Run docker compose to perform a local test
docker-compose up -d
```

## Minikube

### Starting with the basics
Let's dig into Minikube. Afterwards, you'll find the first commands to import the image created above and to run it
within Minikube.
```
// Starts Kubernetes
minikube start 

// Copy a local image into minikube
minikube image load minikube-example:latest

// Check all available images in minikube
minikube image ls --format table

// Provide a yaml example to be used within a deployment.yaml
kubectl create deployment first-k8s-deploy \                                                                 
  --image="docker.io/library/minikube-example" \
  -o yaml \
  --dry-run 

// Creates a deployment.
kubectl apply -f ./minikube/minikube_deployment.yaml

// Creates a service for external reachability
kubectl apply -f ./minikube/minikube_loadbalancer_service.yaml

// Make the service accessible (-> open in Browser)
kubectl get services

// Stop minikube
minikube stop

// Delete all files corresponding to the Kubernetes cluster
minikube delete -o
```

### Services Deep-Dive

Source: https://www.baeldung.com/ops/kubernetes-service-types

### Service Types in Kubernetes
Let’s take a closer look at the different service types.

#### ClusterIP Services

ClusterIP is the default service type in Kubernetes, and it provides internal connectivity between different components
of our application. Kubernetes assigns a virtual IP address to a ClusterIP service that can solely be accessed from 
within the cluster during its creation. This IP address is stable and doesn’t change even if the pods behind the service
are rescheduled or replaced.

ClusterIP services are an excellent choice for internal communication between different components of our application 
that don’t need to be exposed to the outside world. For example, if we have a microservice that processes data and sends
it to another microservice for further processing, we can use a ClusterIP service to connect them.

To create a ClusterIP service in Kubernetes, we need to define it in a YAML file and apply it to the cluster. Here’s an
example of a simple ClusterIP service definition:
```
apiVersion: v1
kind: Service
metadata:
  name: backend
spec:
  selector:
    app: backend
  ports:
  - name: http
    port: 80
    targetPort: 8080
```
In this example, we define a service named backend with a selector that targets pods labeled with app: backend.
The service exposes port 80, which is the port used by clients to access the service, and forwards the traffic to the
pods’ port 8080, which is where the backend application is running.

#### NodePort Services

NodePort services extend the functionality of ClusterIP services by enabling external connectivity to our application.
When we create a NodePort service on any node within the cluster that meets the defined criteria, **Kubernetes opens up
a designated port that forwards traffic to the corresponding ClusterIP service running on the node.**

These services are ideal for applications that need to be accessible from outside the cluster, such as web applications
or APIs. With NodePort services, we can access our application using the node’s IP address and the port number assigned
to the service.

Let’s look at an example of a simple NodePort service definition:
```
apiVersion: v1 
kind: Service 
metadata: 
  name: frontend 
spec: 
  selector: 
    app: frontend 
  type: NodePort 
  ports: 
    - name: http 
      port: 80 
      targetPort: 8080
```
We define a service named frontend that targets pods labeled with app: frontend by setting a selector. The service
exposes port 80 and forwards the traffic to the pods’ port 8080. We set the service type to NodePort, and Kubernetes
exposes the service on a specific port on a qualifying node within the cluster.

When we create a NodePort service, Kubernetes assigns a port number from a predefined range of 30000-32767.
Additionally, we can specify a custom port number by adding the nodePort field to the service definition:
```
apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  selector:
    app: frontend
  type: NodePort
  ports:
  - name: http
    port: 80
    targetPort: 8080
    nodePort: 30080
```
The nodePort field is specified as 30080, which tells Kubernetes to expose the service on port 30080 on every node in 
the cluster.

#### LoadBalancer Services
LoadBalancer services connect our applications externally, and production environments use them where high availability
and scalability are critical. When we create a LoadBalancer service, **Kubernetes provisions a load balancer in our
cloud environment and forwards the traffic to the nodes running the service.**
LoadBalancer services are ideal for applications that need to handle high traffic volumes, such as web applications or
APIs. With LoadBalancer services, we can access our application using a single IP address assigned to the load balancer.

Here’s an example of a simple LoadBalancer service definition:
```
apiVersion: v1
kind: Service
metadata:
  name: web
spec:
  selector:
    app: web
  type: LoadBalancer
  ports:
    - name: http
      port: 80
      targetPort: 8080
```
We set the service type to LoadBalancer to instruct Kubernetes to provision a load balancer. Here, we define a service
named web and specify a selector that targets pods labeled with app: web. Additionally, we expose port 80 and forward
traffic to the pods’ port 8080.

After creating the LoadBalancer service, Kubernetes provisions a load balancer in the cloud environment with a public
IP address. We can use this IP address to access our application from outside the cluster.

#### Conclusion
| Service Type | Use Case                                              | Accessibility                                                            | Resource Allocation          |
|--------------|-------------------------------------------------------|--------------------------------------------------------------------------|------------------------------|
| ClusterIP    | Internal communication between application components | Within the cluster only                                                  | Minimal resources needed     |
| NodePort     | External accessibility for web applications or APIs   | Accessible from outside the cluster via a high-numbered port on the node | Additional resources needed  |
| LoadBalancer | Production environments with high traffic volumes     | Accessible from outside the cluster via a load balancer                  | Significant resources needed |

### Further Scaling

The biggest benefit of Kubernetes is the possibility to scale the applications even further.
To do so, we have to change the amount of pods of our deployment:
```

```